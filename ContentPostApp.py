import webapp


class contentPostApp(webapp.webapp):

    recursos = {'/':'Homepage',
               '/Chris':'Page of Ch'}

    def parse(self, recived):
        method = recived.decode().split(' ')[0]
        resource = recived.decode().split(' ')[1][1:]
        if method == "POST":
            body = recived.decode().split('\r\n\r\n')[1]
        else:
            body = None
        return method, resource, body

    def process(self, analyzed):
        method, resource, body = analyzed

        if method == "POST":
            self.recursos[resource] = f"Page of {resource}"
        if resource in self.recursos.keys():
            http = '200 OK'
            html = '<html><body><h1> ' \
                   + self.recursos[resource] + '.<br> Your request is saved and is: ' + resource + \
                   '</h1>'
        else:
            http = '200 OK'
            html = "<html><body><h1>" + \
                   f"Your request {resource} cannot be saved in the dictionary." \
                   "</h1>"
        resources_list = ""
        for key in self.recursos.keys():
            resources_list += f"{key} <br>"
        html += '<form method="post">' \
                '<label>Enter a resource:' \
                '<input name="submitted-name" autocomplete="name"/>' \
                '</label>' \
                '<button>SAVE!</button>' \
                '</form>' \
                '<h1>List of saved resources:</h1><br>' \
                + resources_list + \
                '</body></html>'
        return http, html


if __name__ == "__main__":
    ContentPostapp = contentPostApp('localhost', 1234)